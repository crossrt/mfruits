<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
	'as' => 'index',
	'uses' => 'MainController@index'
]);

Route::post('/upload', [
	'as' => 'upload',
	'uses' => 'MainController@upload'
]);

Route::get('/result/{token}', [
	'as' => 'result',
	'uses' => 'MainController@result'
]);

Route::get('/search', [
	'as' => 'search',
	'uses' => 'MainController@search'
]);

Route::get('/test', [
	'as' => 'test',
	'uses' => 'MainController@test'
]);

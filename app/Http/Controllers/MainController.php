<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Log;

use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;

use App\Fruit;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function upload(Request $request)
    {
        /* exit application if current request is not POST method */
        if(!$request->isMethod('post')) {
            abort(404);
        }

        /* exit application if no image uploaded */
        if(!$request->hasFile('image')) {
            abort(404);
        }

        $image = $request->file('image');                       // retrieve image from request
        $filename = $image->getClientOriginalName();            // get original file name
        $image->move(public_path() . '/images', $filename);     // move image from temp to public folder

        /* setup new request to submit image and its parameters */
        $client = new GuzzleHttp\Client();
        $res = $client->post('http://api.cloudsightapi.com/image_requests', [
            'headers' => [
                'Authorization' => 'CloudSight ' . env('API_KEY') // submit API key to CloudSight
            ],
            'form_params' => [
                'image_request[remote_image_url]' => url().'/images/'. $filename, // location of the fruit image
                'image_request[locale]' => 'en-US'
            ]
        ]);
        $result = json_decode($res->getBody(), true); // retrieve result from response of the request sent to CloudSight

        /* if any errors found in result, log it and exit application */
        if(isset($result['error'])) {
            Log::info($result);
            abort(503);
        }

        /* bring user to result page with token from result */
        return redirect()->route('result', [$result['token']]);
    }

    public function result($token)
    {
        /* exit application if no token found */
        if($token=='') {
            abort(404);
        }

        /* Setup new request to get image result */
        $client = new GuzzleHttp\Client();
        $res = $client->get('http://api.cloudsightapi.com/image_responses/'.$token, [
            'headers' => [
                'Authorization' => 'CloudSight ' . env('API_KEY')
            ]
        ]);
        $result = json_decode($res->getBody(), true);

        /* Check result status */
        $status = $result['status'];
        if($status==='not completed')
            return view('pending')->with('token', $token);  // pending page will refresh every 5 seconds
        else if($status==='skipped' || $status!=='completed')
            return view('error')->with('result', $result);   // error page will display its reason.

        /* data returning to result page */
        $data = [];
        $data['result'] = $result;

        /* find fruit in database */
        $fruits = Fruit::all();
        foreach ($fruits as $fruit) {
            if( strpos($result['name'], $fruit->name) !== false ||  strpos($fruit->name, $result['name']) !== false ) {
                $data['fruit'] = $fruit;
                break;
            }
        }

        /* if no fruit found */
        if(!isset($data['fruit'])) {
            return view('result');
        }

        /* parse fruit's coordinates in order to display on Google Map */
        $data['coordinates'] = [];
        $coordinates = explode(';', $data['fruit']['coordinate']);
        foreach ($coordinates as $coordinate) {
            $coordinate = explode(',', $coordinate);
            $lat = $coordinate[0];
            $lng = $coordinate[1];
            $coordinate = [
                'lat' => (double) $lat,
                'lng' => (double) $lng
            ];
            array_push($data['coordinates'], $coordinate);
        }

        /* show user result if no errors found */
        return view('result', $data);
    }

    public function search(Request $request)
    {        
        $keyword = $request->get('keyword');
        $keyword = trim($keyword);
        $fruit = Fruit::where('name', 'LIKE', "%{$keyword}%")->first();
        if(!$fruit) {
            return view('result');
        }

        $data = [];
        $data['fruit'] = $fruit;

        $data['coordinates'] = [];
        $coordinates = explode(';', $data['fruit']['coordinate']);
        foreach ($coordinates as $coordinate) {
            $coordinate = explode(',', $coordinate);
            $lat = $coordinate[0];
            $lng = $coordinate[1];
            $coordinate = [
                'lat' => (double) $lat,
                'lng' => (double) $lng
            ];
            array_push($data['coordinates'], $coordinate);
        }

        return view('result', $data);

    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFruitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fruits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description', 1000);
            $table->string('taste', 1000);
            $table->string('handle', 2000);
            $table->string('value', 2000);
            $table->string('benefits', 5000);
            $table->string('season');
            $table->string('suggestion', 2000);
            $table->string('how', 5000);
            $table->string('coordinate', 1000);
            $table->string('filename');
        });

        DB::table('fruits')->insert([
            [
                'name' => 'sapodilla',
                'description' => 'Round to egg shape fruit with 2-4 inches in diameter. It has brown skin & scruffy when ripe.',
                'taste' => 'Sweet taste like caramel & smooth or grainy texture with slight musky flavor.',
                'handle' => 'Peel off the / cut into half first and then remove the seeds.',
                'value' => 'It contains Vitamin A, Vitamin C & Fiber',
                'benefits' => '1. Good for digestion & used in constipation treatment.<br>2. Vitamin A help to ensure proper vision, maintain healthy skin & mucus membranes.<br>3. Vitamin C helps body to against pathogens with a resistance development & to destroy free radicals.<br>4. Helps in cold & cough treatment, to remove nose blockage, also in case of persistent coughs.',
                'season' => 'January to February & May to July',
                'suggestion' => 'Strongly advice diabetic not to consume much.',
                'how' => 'Choose a smooth fruit with intact skin that has no cracks, bruises or wrinkles.',
                'coordinate' => '3.17651,101.666079;3.256008,101.658747',
                'filename' => 'sapodilla.jpg',
            ],
            [
                'name' => 'snake fruit',
                'description' => 'Scaly, brown skinned fruit which grows in clusters at the base of the plant.',
                'taste' => 'Creamy yellow color fruit with a sweet acid taste rather like pineapple, but is crisp & crunchy.',
                'handle' => 'To eat, remove the stem & peel back the scaly brown skin.',
                'value' => 'Consist of nutrition like Protein, Beta-Carotene, Vitamin-C, and Thiamine Dietary fiber, Iron, Calcium, Phosphorus, Carbohydrates and rich in Potassium and also Pectin which are great for overall health.',
                'benefits' => '1. Maintain eye health as it contain beta-carotene which is good to eye.<br>2. The bark has high fiber enable to treat diarrhea.<br>3. With the content of phytonutrients that exist in the bark can help on diet.',
                'season' => 'Harvested & sold year around',
                'suggestion' => 'Don\'t eat too much as it might cause constipation',
                'how' => 'Look for a snake fruit with a little heavy weight & complete skin.',
                'coordinate' => '3.175223,101.689708',
                'filename' => 'snake-fruit.jpg',
            ],
            [
                'name' => 'pomelo',
                'description' => 'Pomelo is a large citrus fruits are usually yellow, pale or light green in appearance, while it edible flesh is pulpy and white.',
                'taste' => 'Taste like a sweet, mild grapefruit  but without bitterness or sour flavor.',
                'handle' => '1. Cut the "ca" off of the pomelo.<br>2. Cut vertical slices down, about 1/2 inch into the rind.<br>3. Pull the slices off the fruit.<br>4. Pull the "rind flower" off from the fruit.<br>5. Breaking it into slices & remove the membrane surrounds each slice.',
                'value' => 'Consist of vitamins, minerals, and organic compounds like vitamin C, potassium, dietary fiber, vitamin B6, and magnesium.',
                'benefits' => '1. Bone Health, pomelos can aid in preventing osteoporosis and general bone weakness throughout the body.<br>2. Anti-aging, high vitamin C intake can prevent signs of premature aging like wrinkles, sagging skin, and age spots. <br>3. Weight Loss: pomelos contain a “fat-burning enzyme” called Carnitine pamitoyl-transferase that can help you redu ce your weight.<br>4. Immune System: Vitamin C acts as an antioxidant substance to increase white blood cell activity and attack free radicals that can damage the organ systems of the body. It helps to fight off infections that lead to colds, coughs, fevers, and more serious symptoms or microbial, viral, and bacterial infections.',
                'season' => 'Between November to March',
                'suggestion' => '-',
                'how' => 'Pick a ripe pomelo with heavy size without soft spots or bruises. Also, pick the fruit which have a subtle & sweet fragrance.',
                'coordinate' => '3.155683,101.657436;1.523308,103.662559;1.513012,103.668052;3.074215,101.743516',
                'filename' => 'pomelo.jpg',
            ],
            [
                'name' => 'guava',
                'description' => 'Guavas, AKA Apple guavas received its name because its coloring is so similar to that of a green apple, with hues of lemon and lime. The fruit is roughly spherical with a furrowed smooth surface. The most alluring element of a Guava is its fragrance, a bouquet of bright tropical aromatics. The flesh is unique to many fruits, its texture a combination of a firm banana with the succulence of an apple.',
                'taste' => 'Guavas are said to taste like a combination of a pear and a strawberry. The flavor of Guava is a reflection of its aromatics, though more subtle with notes of pineapple, papaya, banana and lemon.',
                'handle' => 'Guava can either eat whole or as follow:<br>1. Prepare a ripe guava which has gone from bright green to a softer yellowish-green color.<br>2. Wash the guava and cut in half.<br>3. Scoope out seeds with spoon.<br>4. Slices into desired size and enjoy the guava.',
                'value' => 'It has a higher nutritional content, such as vitamin C, potassium, and iron, free of saturated fatty acids and sodium, low fat and energy, but high in fiber.',
                'benefits' => '1. Diabetes: The high level of dietary fiber in guava helps to regulate the absorption of sugar by the body, which decreases the chances of major spikes and drops in insulin and glucose in the body. <br>2. Eyesight: Guavas are extremely good sources of vitamin-A, which is well known as a booster for vision health. It can help slow down the appearance of cataracts, macular degeneration, and general health of the eyes. It\'s not only prevent degradation of eyesight, but even an improvement in eyesight once it has begun to degrade.<br>3. Thyroid Health: Guavas are a good source for copper, which is an important part of regulating thyroid metabolism by helping to control hormone production and absorption. The thyroid gland is one of the most important glands in the body for regulating hormones and organ system function, so guava can help balance your health in many ways. <br>4. Constipation: Guava is one of the richest sources of dietary fiber in terms of fruit. Its seeds, if ingested whole or chewed, serve as excellent laxatives. These two properties of guava help the formation of healthy bowel movements, and aid the body in retaining water and thoroughly cleaning your intestines and excretory system. It is said that constipation alone can lead to 72 different types of ailments, so any help with constipation is beneficial. <br>5. Brain Health: Another of the tremendous positive benefits of guavas is the presence of B3 and B6 vitamins. B3 (also known as niacin) can increase blood flow and stimulates cognitive function. B6 is a great nutrient for brain and nerve function. Therefore, eating guava can help you increase brain function and sharpen you focus.<br>6. Cough & cold: Juice of raw and immature guavas or a decoction of guava-leaves is very helpful in relieving coughs and colds by  reducing mucus, disinfecting the respiratory tract, throat and lungs, and inhibiting microbial activity with its astringent properties.<br>7. Skin care: Guavas can improve the texture of your skin and help you to avoid skin problems more than even the highest ranked beauty creams or skin toner gels. This is chiefly due to the abundance of astringents in the fruit (more astringent is present in immature guavas) and in its leaves. Your skin can benefit from either eating the fruits (this helps tighten your muscles apart from your skin) or by rinsing your skin with a decoction of its immature fruit and leaves.',
                'season' => 'Generally there are two crops per year which is between September to October and March to April period. However, its available all year around',
                'suggestion' => 'Generally there are two crops per year which is between September to October and March to April period. However, its available all year around',
                'how' => '1. Look for a softer guava as it will be the sweeter & more delicious. Moreover, choose the guava that is blemish-free. <br>2. Squeeze it gently & its ripe if it gives under your fingers.<br>3. Smell the guava. A perfectly ripe guava is one you can smell without even putting it to your nose.',
                'coordinate' => '3.082490,101.685467;2.190897,102.262446',
                'filename' => 'guava.jpg',
            ],
            [
                'name' => 'durian',
                'description' => 'King of Tropical Fruit\' Durian described as strong & unique smell  and tastes like heaven for those who like and love them. Usually durian is either loved at the first taste, or hated.',
                'taste' => 'Large and covered in spines with a really creamy flesh with either sweet ot bitter sweet taste. It\'s abstract to describe durian\'s taste, it\'s just the taste of durian and you have to try it!',
                'handle' => 'It is usually easiest to begin at the top, near the stem. Each pod, when ripe, has a natural seam along the pod, from the top to the bottom. <br>1. Cut off the stem to make the durian easier to handle.<br>2. Flip the durian stem-side down. With help of a hand towel or cloth to hold it in place. <br>3. Place the knife directly at the center of the bottom. Push down until the knife enters to the handle. <br>4. Twist the knife back and forth. The seams will (probably) rupture.<br>5. Put the knife down, and grasp two sides of the durian with your fingers inside the opening. Pull the durian in half using brute force. This part can sometime take some serious effort. If it\'s too difficult, cut along the seams to the stem.  <br>6. If all has gone well, you have neatly parted your durian into two halves, displaying two full sections of luscious, gleaming durian custard.<br>7. Eat everything visible. Now, you need to open the remaining sections. Place both hands, fingers facing outward and palms inside the smooth white pods, on either side of the durian on the end farthest from the stem. Lock your elbows, and push down using your body weight to split the pod open.',
                'value' => 'It contains fiber, vitamins such as vitamin-C, folic acid, thiamin, riboflavin, niacin, B6, vitamin A and important minerals such as potassium, iron, calcium,  magnesium, sodium, zinc, phosphorus are found in durian.',
                'benefits' => '1. Digestive aid as fiber causes bowel movement to increase in bulk, to reduced indigestion, heartburn, cramps & cholesterol in blood. Its\' insoluble fiber also lower the frequency diarrhea of people.<br>2. Durian is a rich source of potassium, and since potassium is such an integral part of the salt and fluid balance throughout the cells of the body, potassium levels also dictate blood pressure.<br>3. Reduced stress on the veins and arteries also increases oxygenated blood flow to the brain, and studies have shown that potassium levels can therefore help boost cognitive function and memory, reducing the risk of developing Alzheimer’s and dementia!<br>4. Anti-aging: In traditional, herbal medicine, durian was often hailed as a tool to battle aging, and was one of the main reasons why people call it the “king of fruits”. It has a wide variety of antioxidant properties stemming from its vitamin and organic chemical makeup that actively reduce the amount of free radicals in the body. Eating an excessive amount of durian can seriously boost your body’s ability to eliminate those free radicals, thereby reducing the chances of premature aging and delaying the appearance of symptoms such as wrinkles, age spots, macular degeneration, hair loss, tooth loosening, arthritis, cancer, and heart disease.<br>5. It helps to battle insomnia.',
                'season' => 'Middle of the year around May to June',
                'suggestion' => 'Don\'t take too much as it may cause weight gain<br>Don\'t consume with alcohol as serious will cause death',
                'how' => 'Look for a light brown fruit with spikes. Dark or white patches are signs of under or overripe fruit and should not be picked.',
                'coordinate' => '3.082490,101.685467;3.074215,101.743516',
                'filename' => 'durian.jpg',
            ],
            [
                'name' => 'dragon fruit',
                'description' => 'with pink peel, green scales, and either pink or white flesh',
                'taste' => 'Sweet and very juicy like honey and sugar.',
                'handle' => '1. Wash the fruit.<br>2. Cut the dragon fruit in half with a sharp knife to slice right through it.<br>3. Scoop out the flesh with a spoon & it should be easily ro separate from the skin.',
                'value' => 'High in fiber, vitamin C , they are rich in phosphorus and calcium and are free radical fighters known to contain phytoalbumin antioxidants and also have been shown to lower cholesterol and blood pressure.',
                'benefits' => '1. helps strengthen your immune system and promotes faster healing of bruises and wounds.<br>2. packed with minerals such as calcium for stronger bones and teeth, phosphorus for tissue formation and iron for healthy blood.<br>3. High in fiber, regular consumption can help avoid constipation, improve your digestive health and help to reduce weight.',
                'season' => 'Fruit ripe by July to August or up to December(depend on the daily weather)',
                'suggestion' => 'Don\'t eat any dragon fruit skin as the skin is inedible, and could give you a stomachache if you do so.',
                'how' => '1. Pick the ripe Dragon fruit which is bright red or pink. Just like a kiwi or a peach, it tastes best when it\'s fully ripe.<br>2. If it has a little give when press the flesh, it is probably ripe. If it is too soft, that means it is overripe, and the texture will not be as good. If it is hard, give it a few days before eat it.',
                'coordinate' => '4.586141,101.136294;4.565007,101.049440',
                'filename' => 'dragon-fruit.jpg',
            ],
            [
                'name' => 'pineapple',
                'description' => 'Pineapples have a wide cylindrical shape, with a scaly green, brown or yellow skin and a regal crown of spiny, green leaves. It has yellow in color fibrous flesh.',
                'taste' => 'It taste is sweet and tart.',
                'handle' => '1. Cut the top green leaves with a straight slice.<br>2. Cut off the bottom of pineapple with a straight cut as well to expose the interior.<br>3. Remove the outer husk off, into a nearly squarish shape and you will see the "eyes" left.<br>4.Cut the pieces through the middle, and slice from the side according to your desired size.',
                'value' => 'It wealth of nutrients, vitamins, and minerals, including potassium, copper, manganese, calcium, magnesium, vitamin C, beta carotene,  thiamin, B6, and folate, as well as soluble and insoluble fiber, and bromelain.',
                'benefits' => '1. Blood circulation: pineapples provide the body with copper, another essential mineral that functions in a number of enzymatic reactions and compounds in the body. <br>2. Blood Pressure: One of the most important functions of potassium is as a vasodilator, meaning that it eases the tension and stress of the blood vessels and promotes blood circulation to various parts of the body. When your blood vessels relax, your blood pressure is reduced and the flow of blood is less restricted. <br>3. Eye Health: Macular degeneration affects many elderly people, and beta carotene can help to delay this vision problem.<br>4. Oral Health: Along with the antioxidant compounds that protect against oral cancer, pineapples also have astringent properties, which strengthen gums and make sure that your teeth do not become loose. <br>5. Tissue and Cellular Health: One of the commonly overlooked benefits of vitamin C is its essential role in creating collagen. High vitamin C content helps you heal wounds and injuries to the body quickly, along with defending against infections and illness.<br>6. Cancer Prevention: In addition to the antioxidant potential of vitamin C in the battle against cancer, pineapples are also rich in various other antioxidants, including vitamin A, beta carotene, bromelain, various flavonoid compounds, and high levels of manganese, which is an important co-factor of superoxide dismutase, an extremely potent free radical scavenger that has been associated with a number of different cancers. Pineapple has directly been related to preventing cancers of the mouth, throat, and breast.',
                'season' => 'Available year around, and peak season is from March to July.',
                'suggestion' => 'Don\'t take too much pineapple as an “overdose” can lead to vomiting, nausea, diarrhea, headaches, and nausea.<br>Also, bromelain has been known to stimulate menstruation, so pregnant women should avoid excessive pineapple, as the high levels of bromelain can actually lead to a miscarriage in rare cases.',
                'how' => 'Pick a ripe pineapple with a fragrant sweet smell at the stem end. Avoid to take a sour, fermented, or musty smells pineapple.',
                'coordinate' => '4.598330,101.064170;3.098915,101.677683',
                'filename' => 'pineapple.jpg',
            ],
            [
                'name' => 'banana',
                'description' => 'Not all bananas are long and yellow. Some are small, wide, or even different-colored. Red bananas are smaller, softer, and sweeter than common Cavendish bananas, with a slight raspberry flavor.',
                'taste' => 'It has sweet and unique flavor.',
                'handle' => '1. Look for a ripe banana that is yellow in color and lightly speckled with small brown or black speckles. <br>2. Peel the banana either from the stem end or from the blossom (non-stem) end.',
                'value' => 'Contain potassium, pectin, magnesium, vitamins C, B6, and high in antioxidants.',
                'benefits' => '1. Heart Health: Bananas are good for your heart. They are packed with potassium, a mineral electrolyte that keeps electricity flowing throughout your body, which is required to keep your heart beating. <br>2. Depression and mood: Bananas can be helpful in overcoming depression due to high levels of tryptophan, which the body converts to serotonin, the mood-elevating brain neurotransmitter. Plus, vitamin B6 can help you sleep well, and magnesium helps to relax muscles.<br>3. Bones: Bananas contain an abundance of fructooligosaccharides. These are nondigestive carbohydrates that encourage digestive-friendly priobotics and enhance the body\'s ability to absorb calcium.<br>4. Cancer: Bananas may be helpful in preventing kidney cancer because of their high levels of antioxidant phenolic compounds.',
                'season' => 'Available year around.',
                'suggestion' => 'Bananas are a sugary fruit, so eating too many and not maintaining proper dental hygiene practices can lead to tooth decay.',
                'how' => '1. Look for bananas with no brown spots or obvious signs of handling damage. Large brown spots indicate an over-ripe, mushy or even fermented banana.<br>2. Consider buying smaller bunches bananas of various stage ripeness.',
                'coordinate' => '3.153080,101.697252;5.420863,100.341560',
                'filename' => 'banana.jpg',
            ],
            [
                'name' => 'mangosteen',
                'description' => 'Known as the "Queen of Tropical Fruits". It come with four greenish caps at the top, and dark-purple or nearly black in color. Turn the mangosteen upside down & you will see the brown raised ridges that look like flowers & the number of the "petals" corresponds with the sections number in each mangosteen.',
                'taste' => 'It taste described as sweet, mildy tangy, fragrant, and delicious.',
                'handle' => '1. Cut a mangosteen with a sharp paring knife by make a 1/4 to 1/3 inch deep horizontal cut of it shell.<br>2. Or it can open without a knife by hold the mangosteen between your palms & squeeze your hands together to crush open the purple shells.<br>3. Enjoy the white exposing, creamy flesh.',
                'value' => '1. Mangosteen is a healthy fruit which is rich in water, energy, protein, carbohydrate, fiber and have essential nutrients such as calcium, iron, magnesium, phosphorus, potassium, sodium, zinc, copper and manganese are found in this fruit. <br>2. It contains vitamin C, B6, B12 and A which are required for maintaining good health. Other vitamins  are also found in mangosteen such as thiamin, riboflavin, niacin, pantothenic acid, folate, folic acid, carotene and cryptoxanthin.',
                'benefits' => '1. Anti-cancer properties: The pericarps of mangosteen contain xanthones which exhibit anti-cancer, anti-inflammatory and anti-bacterial effects.<br>2. Anti-inflammatory properties: Mangosteen have anti-allergy and anti-inflammatory properties and they inhibit the release of histamine and prostaglandin which are associated with inflammation in the human body.<br>3. Healthy skin: Mangosteen helps in maintaining healthy skin. The anti-inflammatory, anti-bacterial, anti-fungal, anti-allergy and anti-oxidant properties help in reducing risk of various conditions such as skin inflammation, skin aging, eczema, allergies and bacterial infections. <br>4. Medicinal use: Mangosteen with its anti-bacterial and anti-inflammatory properties is beneficial as a medicinal drink and for quick healing of wounds.  <br>5. Menstrual problems: Mangosteen root helps in regulating menstrual cycle in women.<br>6. Cardioprotective effect: Mangosteen may also be helpful in reducing risk of stroke or myocardial infarction.<br>7. Diabetes: Mangosteen one natural remedy which is effective in managing and maintaining blood sugar levels in the body<br>8. Diarrhea and dysentery: The mangosteen pericarp and peel are effective in providing relief from stomach disorders such as diarrhea and dysentery.',
                'season' => 'Season is between June to August',
                'suggestion' => 'The fruit in its fresh form should not be an issue for diabetics unless it is eaten in excessive amounts.',
                'how' => '1. Select mangosteen with greenish caps at the top.<br>2. Pick the one with a little soft purple shell. ',
                'coordinate' => '3.129872,101.713948;3.074215,101.743516',
                'filename' => 'mangosteen.jpg',
            ],
            [
                'name' => 'papaya',
                'description' => 'Papaya is yellow-green in appearance and the flesh is often orange or red in color.',
                'taste' => 'It have a mild sweet taste with not very distinctive.',
                'handle' => '1. Papaya can eaten at room temperature and is the best when served cold. Place a whole papaya into the frige. <br>2. Slice the papaya in half.<br>3. Scoop out the black seeds with a spoon.<br>4. Enjoy the papaya with a spoon or cut it into desired size and eat with fork.',
                'value' => 'Papaya is rich in antioxidant nutrients such as carotenes, flavonoids and vitamin C, as well as vitamin B (folate and pantothenic acid), also a good source of fiber and minerals such as magnesium.',
                'benefits' => '1. Helps ease menstrual pain: Women who experience menstrual pain should help themselves to several servings of papaya, as an enzyme called papain helps in regulating and easing flow during menstrual periods.<br>2. Promotes hair growth: Vitamin A in them is utilised in production of sebum, a compound that is crucial for keeping the hair smooth, shiny and moisturised.<br>3. Skin Care: Papayas can be great revitalizing agents, it used in homemade face masks by many women. Papain kills dead cells and purifies the skin. The peels of papayas are also used to rub on the face and hands for healthy skin; it will work wonders for your skin.<br>4. Reduce Acne and Burns: The latex obtained from papayas is used to treat the areas on the skin affected with acne. <br>5. Treating Constipation: The presence of folate, vitamin C and vitamin E in papayas reduces motion sickness by producing a tonic effect in the stomach and in the intestines.<br>6. Arthritis: Papayas have been known to be effective against rheumatoid arthritis and osteoarthritis. One of the enzymes found in papayas, called chemopapain, has a significant effect on controlling rheumatoid arthritis and osteoarthritis.<br>Interesting fact: The seeds can be found application as anti-inflammatory, anti-parasitic, and analgesic, and used to treat stomachache, and ringworm infections.',
                'season' => 'Available about all year round.',
                'suggestion' => '1. Gastrointestinal symptoms may be a side effect of eating too much papaya.<br>2. People who eat too much papaya and ingest high levels of papain may develop symptoms consistent with hay fever or asthma, including wheezing, breathing difficulties and nasal congestion.<br>3. pregnant women should not use papaya seeds or the enzyme rich green papaya. ',
                'how' => '1. Pick a papaya with the color range from yellow.<br>2. Gently press the papaya and it should sink slightly into it. A hard papaya is not ripe and papaya with mushy spots or shriveled is overripe.<br>3. Avoid to buy papaya with mold or mildew at its base.<br>4. Good papaya has a faint, sweet scent near the stem.',
                'coordinate' => '3.076906,101.679272;3.256008,101.658747',
                'filename' => 'papaya.jpg',
            ],
            [
                'name' => 'passion fruit',
                'description' => 'Passion fruit features round to oval shape, 4 to 8 centimeters in diameter, there are two main type which is purple and yellow in color.',
                'taste' => 'It has sweet and tart flavor.',
                'handle' => '1. Wash the passion fruits to prevent any harmful chemicals, bacteria or bugs carries from the skin through the flesh of the fruit.<br>2. Gently cut the passion fruit in half with a knife & serrated knives work best when cutting.<br>3. Scoop the contents out with a spoon.',
                'value' => 'It content of antioxidants, flavonoids, vitamin A, vitamin C, riboflavin, niacin, iron, magnesium, phosphorus, potassium, copper, fiber, and protein.',
                'benefits' => '1. Immune System: This immune strengthening property was due to the presence of vitamin C, carotene, and cryptoxanthin.<br>2. Insomnia: One often overlooked compound in passion fruit is a medicinal alkaloids, including harman, which functions as a sedative. This compound of passion fruit has been connected to a reduction in restlessness, insomnia, sleeplessness, and nervous anxiety which can keep you from getting a good night’s sleep. <br>3. Respiratory Conditions and Asthma: The purple passion fruit peel creates a novel mixture of bioflavanoids, which have an expectorant, sedative, and soothing effect on the respiratory system. It has been positively connected to a reduction in asthma attacks, wheezing, and whooping cough.<br>4. Cancer Prevention: It has powerful source of anti-carcinogenic activity in the body. Antioxidants in passion fruit primarily eliminate free radicals, which are known for mutating the DNA of healthy cells into cancerous ones.',
                'season' => 'Available just about all year round.',
                'suggestion' => '1. Don\'t scrape the skin too hard as the white layer is bitter and tastes bad. <br>2. Do not eat the skin.<br>3. Most of the calories of this fruit do come from sugars, so people with diabetes should be careful to not eat it excessively.',
                'how' => '1. Consider the texture of the passion fruit\'s skin. Pick the ones that are slightly wrinkled and a deep purple color and the one you feel have a lot of liquid when you shake--these are the ones that have ripened the most and will be the sweetest & the softer the shell, the more ripe the fruit will be.<br>2. Smell the fruit & you can smell there is a lot of tropical aromas & it taste will be good.',
                'coordinate' => '3.104332,101.677727;2.190897,102.262446;5.420863,100.341560',
                'filename' => 'passion-fruit.jpg',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fruits');
    }
}

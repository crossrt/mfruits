<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{env('NAME')}}</title>

    <!-- Bootstrap -->
    <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
      body {
        background-color: #039BE5;
        color: white;
        text-align: center;
        padding: 5%;
      }

      .panel {
        color: black;
      }

      .panel-heading a:after {
          font-family:'Glyphicons Halflings';
          content:"\e114";
          float: right;
          color: grey;
      }
      .panel-heading a.collapsed:after {
          content:"\e080";
      }

      .panel-body {
        text-align: left;
      }

      #map {
        height: 400px;
        width: 100%;
      }

    </style>
  </head>
  <body>
    @if(isset($fruit))
      <img src="/fruits/{{$fruit['filename']}}" alt="{{$fruit['name']}}" style="width: 100%;">
      <h1>{{$fruit['name']}}</h1>

      <div class="panel-group" id="accordion">
        <!-- description -->
        <div class="panel panel-default" id="panel-description">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#description" href="#description">
                Description
              </a>
            </h4>
          </div>
          <div id="description" class="panel-collapse collapse in" aria-expanded="true">
            <div class="panel-body">{{$fruit['description']}}</div>
          </div>
        </div>

        <!-- taste -->
        <div class="panel panel-default" id="panel-taste">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#taste" href="#taste" class="collapsed">
                Taste
              </a>
            </h4>
          </div>
          <div id="taste" class="panel-collapse collapse">
            <div class="panel-body">{{$fruit['taste']}}</div>
          </div>
        </div>

        <!-- handle -->
        <div class="panel panel-default" id="panel-handle">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#handle" href="#handle" class="collapsed">
                How to handle
              </a>
            </h4>
          </div>
          <div id="handle" class="panel-collapse collapse">
            <div class="panel-body">{!! $fruit['handle'] !!}</div>
          </div>
        </div>

        <!-- value -->
        <div class="panel panel-default" id="panel-value">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#value" href="#value" class="collapsed">
                Nutritional Value
              </a>
            </h4>
          </div>
          <div id="value" class="panel-collapse collapse">
            <div class="panel-body">{!! $fruit['value'] !!}</div>
          </div>
        </div>

        <!-- benefits -->
        <div class="panel panel-default" id="panel-benefits">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#benefits" href="#benefits" class="collapsed">
                Health Benefit(s)
              </a>
            </h4>
          </div>
          <div id="benefits" class="panel-collapse collapse">
            <div class="panel-body">{!! $fruit['benefits'] !!}</div>
          </div>
        </div>

        <!-- season -->
        <div class="panel panel-default" id="panel-season">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#season" href="#season" class="collapsed">
                Best Season to eat
              </a>
            </h4>
          </div>
          <div id="season" class="panel-collapse collapse">
            <div class="panel-body">{!! $fruit['season'] !!}</div>
          </div>
        </div>

        <!-- suggestion -->
        <div class="panel panel-default" id="panel-suggestion">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#suggestion" href="#suggestion" class="collapsed">
                Do / Don't
              </a>
            </h4>
          </div>
          <div id="suggestion" class="panel-collapse collapse">
            <div class="panel-body">{!! $fruit['suggestion'] !!}</div>
          </div>
        </div>

        <!-- how -->
        <div class="panel panel-default" id="panel-how">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-target="#how" href="#how" class="collapsed">
                How to pick
              </a>
            </h4>
          </div>
          <div id="how" class="panel-collapse collapse">
            <div class="panel-body">{!! $fruit['how'] !!}</div>
          </div>
        </div>
      </div>
      <div id="map"></div>
      <br>
      <a href="/" class="btn btn-success btn-block">Go Back</a>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1k0Yl7NmxIO7--quInfAwVLMlix2ilyE&signed_in=true&callback=initMap"></script>
      <script>
        function initMap() {
          var myLatLng = {lat: 3.080972, lng: 101.643247};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: myLatLng
          });

          var coordinates = [
            @foreach ($coordinates as $coordinate)
              {lat: {{$coordinate['lat']}}, lng: {{$coordinate['lng']}} },
            @endforeach
          ];

          for(i=0; i<coordinates.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(coordinates[i].lat, coordinates[i].lng),
              map: map
            });
          }
        }
      </script>
    @else
      <h1>Fruit is not found in our database.</h1>
    @endif
  </body>
</html>

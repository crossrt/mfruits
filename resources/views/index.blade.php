<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{env('NAME')}}</title>

    <!-- Bootstrap -->
    <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
      body {
        background-color: #039BE5;
        color: white;
        text-align: center;
        padding: 5%;
      }

      .btn-file {
          position: relative;
          overflow: hidden;
          width: 100%;
      }

      .btn-file input[type=file] {
          position: absolute;
          top: 0;
          right: 0;
          min-width: 100%;
          min-height: 100%;
          font-size: 100px;
          text-align: right;
          filter: alpha(opacity=0);
          opacity: 0;
          outline: none;
          background: white;
          cursor: inherit;
          display: block;
      }

    </style>
  </head>
  <body>
    <h1>M Fruit</h1>
    <br>
    <p>Upload your fruit image.</p>
    <form id="form-upload" action="{{route('upload')}}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <span class="btn btn-success btn-lg btn-file">
          Browse <input type="file" name="image">
      </span>
    </form>
    <br>
    <h2>OR</h2>
    <br>
    <p>Search by keyword</p>
    <form action="{{route('search')}}">
      <input class="form-control" type="text" name="keyword" style="margin: 0 auto;" placeholder="Fruit Name">
      <br>
      <button id="btn-search" class="btn btn-success btn-lg btn-block">Search</button>
    </form>
    <h3 style="margin-top:50px;">About</h3>
    <p style="text-align: left;">
      "M Fruits" is a system use to recognize Malaysia's fruits. Malaysia is a Tropicanal Rain Forest country, it has a lot of fruits rich with the benefit of nutrients. Unfortunately, they are not commonly known by everyone, be it local or even foreigners. <br>
      Therefore, it is developed to recognize any tropical fruit images of Malaysia and provide the relevant information, along with its description, taste, nutritional value, and so on. Let's explore to Malaysia's fruit with "M Fruit" now!
    </p>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.btn-file :file').on('change', function() {
            $('#form-upload').submit();
        });

        $('#btn-search').on('click', function(event) {
          var keyword = $('input[name=keyword]').val();
          if(keyword=='') {
            alert('Please enter a keyword of the fruit');
            event.preventDefault();
          }
        });
      })
    </script>
  </body>
</html>
